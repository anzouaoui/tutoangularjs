(function () {
    var app = angular.module('tuto', []);
/**
 * Controller en AngularJS
 */
    app.controller('MainController', ['$scope', '$http', '$filter', 'Membre', function ($scope, $http, $filter, Membre) {
            //$scope.tuto = 'Bonjour � tous';
            //$scope.test
            $scope.membre = Membre.presentation('Dupont', 'Jean');
            $http.get('data.json')
                    .then(function (response) {
                        //Succ�s de la requete
                        $scope.data = $filter('orderBy')(response.data, '+name');
                    });
        }]);

/**
 * Cr�ation d'un service personnel
 */
app.factory('Membre', function(){
    return{
        presentation: function(nom, prenom){
            return 'Bonjour � vous ' + prenom + ' ' + nom + ', comment allez-vous?';
        }
    };
});
})();